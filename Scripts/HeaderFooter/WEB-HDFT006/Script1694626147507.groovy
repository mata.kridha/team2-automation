import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.scrollToElement(findTestObject('Header/Page_Be a Profressional Talent with Coding.ID/div_Site Map'), 2)

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Bootcamp (1)'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Bootcamp dengan Jaminan Kerja di Coding.ID/h2_Programs'), 
    'Programs')

WebUI.back()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Course (1)'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Belajar online kapan saja dan dimana s_6b8099/h2_Kelas Terbaru'), 
    'Kelas Terbaru')

WebUI.back()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Event'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Online event bersertifikat dari prakti_f42b96/h2_Events (1)'), 
    'Events')

WebUI.back()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Kontak (1)'))

WebUI.verifyElementText(findTestObject('Header/Page_Hubungi kami untuk info lebih lanjut -_dfbcce/div1_kontakKami'), 'Kontak Kami')

WebUI.back()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Login'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Masuk untuk dapatkan akses di Coding.ID/span_Masuk'), 
    'Masuk')

WebUI.back()

WebUI.click(findTestObject('Object Repository/Header/Page_Be a Profressional Talent with Coding.ID/a_Sign Up'))

WebUI.verifyElementText(findTestObject('Object Repository/Header/Page_Buat akun dan dapatkan akses di Coding.ID/span_Buat Akun Baru'), 
    'Buat Akun Baru')

WebUI.takeScreenshot()

WebUI.closeBrowser()

