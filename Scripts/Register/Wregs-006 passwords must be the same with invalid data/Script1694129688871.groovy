import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

GlobalVariable.shouldSkipPrecondition = true

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar')

WebUI.setText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 
    'fajar')

WebUI.sendKeys(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 
    'fajar')

WebUI.setText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    '08-Sep-2000')

WebUI.setText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), 
    'fajar20@gmail.com')

WebUI.setText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), 
    '081392432567')

WebUI.setEncryptedText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'), 
    'MtCYEijlymdHex8ZpoFuHA==')

WebUI.setEncryptedText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'), 
    'MtCYEijlymduMbp+Cuty4w==')

WebUI.click(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyElementText(findTestObject('Object Repository/Wlog-006 passwords must be the same with invalid data/Page_Buat akun dan dapatkan akses di Coding.ID/small_kata sandi tidak sama'), 
    'kata sandi tidak sama')

WebUI.takeScreenshot()

WebUI.closeBrowser()

