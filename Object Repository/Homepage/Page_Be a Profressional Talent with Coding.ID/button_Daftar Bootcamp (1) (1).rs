<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Bootcamp (1) (1)</name>
   <tag></tag>
   <elementGuidId>144fa611-8ce6-4a26-b9b5-0e11ec0ddffd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.daftarBootcamp.daftarhoverbootcamp</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>ca909dc0-7d67-4e4d-b056-9bcebf13c64a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>fb76123e-7dad-4ca0-8ff3-69a98c09a31c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>$store.dataForm.changeClassShow(2);$store.dataForm.classId = 4;$store.dataForm.changeClass()</value>
      <webElementGuid>95ac265f-2af4-4881-938f-24b0920bfdbc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarhoverbootcamp</value>
      <webElementGuid>fc224438-5489-477d-9f64-50b6e5e3f00a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        Daftar Bootcamp
                                    </value>
      <webElementGuid>892e8d46-efda-4de9-95fb-cd52e29e2d92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;tooltip-right&quot;]/button[@class=&quot;daftarBootcamp daftarhoverbootcamp&quot;]</value>
      <webElementGuid>e66d0c60-8801-494c-b1f2-815fbbf50fa1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      <webElementGuid>9e6c409d-b67f-455a-a9f3-6f95871953ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Later'])[1]/following::button[1]</value>
      <webElementGuid>5885bd60-701c-4b30-880f-38cf0844bcd4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[2]/preceding::button[1]</value>
      <webElementGuid>b102dfb4-c1f2-4220-830f-af3d8b01570d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[3]/preceding::button[1]</value>
      <webElementGuid>82a17fab-f665-4158-a803-39e729e9a11c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Bootcamp']/parent::*</value>
      <webElementGuid>2444ce44-9c8e-4e18-9e26-12c5916018d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/button</value>
      <webElementGuid>10ffbad8-3c5e-495b-9d58-c06bdf04651d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                        Daftar Bootcamp
                                    ' or . = '
                                        Daftar Bootcamp
                                    ')]</value>
      <webElementGuid>f6b45c3f-a6c4-4858-9fb2-f256c6e929be</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
