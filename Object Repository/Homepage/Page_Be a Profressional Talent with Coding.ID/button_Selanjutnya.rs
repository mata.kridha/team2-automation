<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Selanjutnya</name>
   <tag></tag>
   <elementGuidId>00db30a6-52bb-4388-9dc2-92716499b356</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#next-button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div[6]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>36f14d97-60d8-47ef-82a9-ebc4f67ceb9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-on:click</name>
      <type>Main</type>
      <value>$store.dataForm.validatePage($store.dataForm.activePage)</value>
      <webElementGuid>aec854d8-1cc0-42db-8faa-3179ddc951f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>next-button</value>
      <webElementGuid>ae655e4c-2209-4727-aa59-d6a5f97e267e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarBootcamp--see-all disabled</value>
      <webElementGuid>7287b50b-f387-4224-bd92-a11bc68a4f35</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>:class</name>
      <type>Main</type>
      <value>$store.dataForm.classId == 0 ||
                            ($store.dataForm.activePage == 1 &amp;&amp; !$store.dataForm.validPage2) || ($store.dataForm
                                .activePage == 2 &amp;&amp; !$store.dataForm.validPage3) ?
                            'disabled' : ''</value>
      <webElementGuid>1140cab8-46d1-4194-b7c8-33f6f02a8025</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Selanjutnya</value>
      <webElementGuid>999ba05a-b52f-47be-a8f8-2a6167a0e099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;next-button&quot;)</value>
      <webElementGuid>49edac43-4b34-4ad5-a409-7f6ed84a108d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='next-button']</value>
      <webElementGuid>0e492433-a9a0-4ba5-b5cd-e1a50ddcdc97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[4]/div/button[2]</value>
      <webElementGuid>8ec478ad-d5e7-411c-954b-21867aea9aea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sebelumnya'])[1]/following::button[1]</value>
      <webElementGuid>1ff51f90-f3c2-44c8-bcf4-3ecfd55899c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Kode milik ', &quot;'&quot;, '', &quot;'&quot;, ' diterapkan')])[1]/following::button[2]</value>
      <webElementGuid>039c6e1f-21f5-4ab6-907c-95f95bfc0ae9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Whatsapp'])[1]/preceding::button[1]</value>
      <webElementGuid>4da7b0e4-5b81-4b58-83e1-b7de30b93ba2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perhatian'])[1]/preceding::button[1]</value>
      <webElementGuid>52e5d7e9-d7e8-4ae6-97d3-fe85ad58fbf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Selanjutnya']/parent::*</value>
      <webElementGuid>6b365930-c40d-4fc6-9fc9-6a31d6c9ce85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/button[2]</value>
      <webElementGuid>6fa00a2e-45a1-4844-8efd-e51339d5b63f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'next-button' and (text() = 'Selanjutnya' or . = 'Selanjutnya')]</value>
      <webElementGuid>1d5cd91d-4757-4491-98ce-554ee4b87b70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'next-button' and (text() = '
                        Selanjutnya
                    ' or . = '
                        Selanjutnya
                    ')]</value>
      <webElementGuid>3f5b30ff-551b-4390-8eba-757844273d12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
