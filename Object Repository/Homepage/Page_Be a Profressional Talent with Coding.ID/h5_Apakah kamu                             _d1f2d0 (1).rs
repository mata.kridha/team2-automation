<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Apakah kamu                             _d1f2d0 (1)</name>
   <tag></tag>
   <elementGuidId>97fc0335-e485-4820-ba99-31d212ca5c81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>446acf33-fa0d-4e10-8e40-8ead0ed02c5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>1d00ae89-e909-4f3c-b048-e46ce7100bb7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apakah kamu
                                    memiliki basic programming?*</value>
      <webElementGuid>f644c91a-c645-433a-88e7-850fab75d1cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;block-input-item block-input-item--page-3&quot;]/h5[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>879da964-5ba1-4b3d-8dea-6f587e43b8fd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div/h5</value>
      <webElementGuid>5e7ef900-31fe-41ad-87f4-43f00831a22f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[7]/following::h5[1]</value>
      <webElementGuid>fe536878-0994-4556-b2ce-cb2d981d65d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[2]/following::h5[1]</value>
      <webElementGuid>5bf01618-da19-4052-8156-cfd0ce407442</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ya'])[1]/preceding::h5[1]</value>
      <webElementGuid>23c65d72-d9a4-4ce6-b583-090c20ed1cc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/h5</value>
      <webElementGuid>b5095a23-7cc6-45a6-beeb-ea5a1e51139a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Apakah kamu
                                    memiliki basic programming?*' or . = 'Apakah kamu
                                    memiliki basic programming?*')]</value>
      <webElementGuid>f47aaf1b-19ef-492d-95a5-cb334760eec3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
