<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Nomor Whatsapp</name>
   <tag></tag>
   <elementGuidId>eaa81903-ea4a-4966-a9c9-17dc9c2ad028</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.block-input-item > h5.new_body2_regular</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>f31299a1-9a33-4477-bf1d-169f4c21b7f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>432f6bcf-42a0-4e2f-969b-88053c427f8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nomor Whatsapp*
                                </value>
      <webElementGuid>943e3533-1339-415d-9485-a5f373885133</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[@class=&quot;block-input-item&quot;]/h5[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>744c143a-bbcd-45d7-abba-3828a5806602</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[2]/div/div/h5</value>
      <webElementGuid>1b79e379-612a-486e-9361-ab3b8c4a8ef4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[6]/following::h5[1]</value>
      <webElementGuid>34fe8c94-1620-43c5-aa48-36e64b1f870e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elka Verso'])[1]/following::h5[1]</value>
      <webElementGuid>d910b1ec-eb07-41ef-b2d0-8d813fb1f8d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Nomor Whatsapp']/parent::*</value>
      <webElementGuid>71d7aa43-96c7-4616-8f5c-2ef0969ca39b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/div/h5</value>
      <webElementGuid>af7d5a77-f4e5-4be7-b2b3-9deee81532f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Nomor Whatsapp*
                                ' or . = 'Nomor Whatsapp*
                                ')]</value>
      <webElementGuid>f2816fa2-3464-4b38-a86c-22871b5cbdca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
