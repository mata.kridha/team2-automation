<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Sebelumnya                             _8fdfb5</name>
   <tag></tag>
   <elementGuidId>348463fa-60fd-4ae2-8f73-df2a64305080</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.button-next-container.space-between</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>45abf81e-1173-4226-a7c5-313c334935de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-show</name>
      <type>Main</type>
      <value>!$store.dataForm.toDahsboard</value>
      <webElementGuid>7518bdaa-f2c5-4fd6-bd0f-6adbd2e042b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button-next-container space-between</value>
      <webElementGuid>0f46e7bd-8938-41ff-a4e6-874482aa42f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Sebelumnya
                    

                    Selanjutnya
                </value>
      <webElementGuid>31759a34-df01-42df-97af-28d7ba0b0611</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;footer-form-block&quot;]/div[@class=&quot;button-next-container space-between&quot;]</value>
      <webElementGuid>78c19650-e5fb-4378-9b10-2799468c433f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[4]/div</value>
      <webElementGuid>655772e0-da91-42cc-ba6d-dc7d180c25b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Kode milik ', &quot;'&quot;, '', &quot;'&quot;, ' diterapkan')])[1]/following::div[2]</value>
      <webElementGuid>0caf61e0-8d24-4091-8920-e83a8dd3b392</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode tidak valid'])[1]/following::div[3]</value>
      <webElementGuid>d5c592ba-29b3-479f-8ffb-2079573bd2fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[4]/div</value>
      <webElementGuid>7524232c-8c3c-412e-bc08-18b571380740</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Sebelumnya
                    

                    Selanjutnya
                ' or . = '
                    
                        Sebelumnya
                    

                    Selanjutnya
                ')]</value>
      <webElementGuid>c64706d9-ddf8-41f7-b93a-57985ae0b42c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
