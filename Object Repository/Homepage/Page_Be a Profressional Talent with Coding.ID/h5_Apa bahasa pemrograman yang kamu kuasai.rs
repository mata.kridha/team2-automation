<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h5_Apa bahasa pemrograman yang kamu kuasai</name>
   <tag></tag>
   <elementGuidId>f1e06405-c02f-441f-98b2-160c7c5097cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/h5</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.skill.dropdown.block-input-item > h5.new_body2_regular</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>c8ca8b1f-3764-4eb8-a5a3-03f6fcb752e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body2_regular</value>
      <webElementGuid>b74f8a81-2e60-497e-98a0-faefad7fbe71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Apa bahasa pemrograman yang kamu kuasai?*
    </value>
      <webElementGuid>ac852967-0f3b-4640-9f2a-137c2ebc7265</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[2]/div[@class=&quot;skill dropdown block-input-item&quot;]/h5[@class=&quot;new_body2_regular&quot;]</value>
      <webElementGuid>55dc419f-9b0b-4674-a1a3-5aad02bf1f13</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/h5</value>
      <webElementGuid>ae743623-0a1c-45b0-aa18-51a08ae58860</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak'])[1]/following::h5[1]</value>
      <webElementGuid>b53dbede-e74e-45c8-8c06-188cb38aac14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ya'])[1]/following::h5[1]</value>
      <webElementGuid>59eb395a-4f78-4f70-ab9e-77b21bc89358</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih skill'])[1]/preceding::h5[1]</value>
      <webElementGuid>382b1289-fc60-4ff2-a3fe-d25f92019d16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apa bahasa pemrograman yang kamu kuasai?']/parent::*</value>
      <webElementGuid>031b82e6-560d-4799-8139-ddfa897cd45b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[2]/div/h5</value>
      <webElementGuid>b3396640-5d70-4a7d-afaf-389761c4032d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Apa bahasa pemrograman yang kamu kuasai?*
    ' or . = 'Apa bahasa pemrograman yang kamu kuasai?*
    ')]</value>
      <webElementGuid>36f72c44-f08b-47a8-89a1-f709ee1ebd3c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
