<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_BOOTCAMP                               _706530</name>
   <tag></tag>
   <elementGuidId>14363585-36e4-4246-b0e1-3a884cd9cc41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@onclick=&quot;window.location.href='https://demo-app.online/bootcamp/quality-assurance-engineer-class';&quot;])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.cardBootcamp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6792b7b4-d401-4584-bdd3-c0b34675250b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cardBootcamp</value>
      <webElementGuid>8e29392e-14b1-4329-b4ca-c2a403f7f801</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>window.location.href='https://demo-app.online/bootcamp/quality-assurance-engineer-class';</value>
      <webElementGuid>e666ee12-ed41-4529-82f6-1b416fad0fee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        </value>
      <webElementGuid>697ed591-2a47-4ccb-88ca-4e6a7f6c1985</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;bootcampBlock&quot;]/div[@class=&quot;blockBootcamp&quot;]/ul[@class=&quot;listBootcamp&quot;]/li[@class=&quot;listBootcampItem&quot;]/div[@class=&quot;cardBootcamp&quot;]</value>
      <webElementGuid>8c9579cb-dc19-4240-833e-f196bb247b86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//div[@onclick=&quot;window.location.href='https://demo-app.online/bootcamp/quality-assurance-engineer-class';&quot;])[2]</value>
      <webElementGuid>9c7cb203-1f26-4204-90ca-92c0e651d8da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div</value>
      <webElementGuid>da356fc6-679d-4157-a9f0-57a809b01340</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Bootcamp'])[1]/following::div[2]</value>
      <webElementGuid>65f9a361-a00a-4613-8173-424e6eb6ef29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li/div</value>
      <webElementGuid>96872dce-ff6a-421c-8473-f7b45e179b5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        ' or . = '
                            
                            
                                BOOTCAMP
                                
                                    Quality Assurance Engineer Class
                                
                                
                                    
                                    
                                        Advance Level
                                    


                                
                                
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.Dalam kelas ini peserta akan...
                            
                        ')]</value>
      <webElementGuid>78d4c9da-2f39-4734-b6d3-e7191deb6f20</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
