<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pilih skill</name>
   <tag></tag>
   <elementGuidId>c82a2f89-06fe-45a1-a9c7-2305d3ca2b5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.skill.dropdown.block-input-item > div.input-form</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>65679965-957d-4ad0-ab0c-059ab8ce789c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions(); $nextTick(() => $refs.inputfocus.focus());</value>
      <webElementGuid>ffd48759-0140-43e3-84f1-14cdbaaa5bb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form</value>
      <webElementGuid>23fc669e-7986-4989-9ab6-c8cbb197dc83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Pilih skill
        </value>
      <webElementGuid>e800c292-7a6c-4eb4-9f82-4e6d6d5c506d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[2]/div[@class=&quot;skill dropdown block-input-item&quot;]/div[@class=&quot;input-form&quot;]</value>
      <webElementGuid>2504cb56-174c-4da9-a9a1-26249e308ea8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>da7aa3f4-94b2-4898-9bda-896dec634979</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[10]/following::div[1]</value>
      <webElementGuid>72f975f1-38c9-4c32-9eb6-d02fbdb8e32b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[11]/preceding::div[3]</value>
      <webElementGuid>25515504-57c3-4861-9d8b-cc6b03cd88ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[12]/preceding::div[5]</value>
      <webElementGuid>7fc5c908-ab2f-45ea-9998-1ec238a40db7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih skill']/parent::*</value>
      <webElementGuid>c8cdddbb-5882-4269-881b-e155b2a2e772</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>d1c34d31-8abf-40f0-b4ec-39ccc2301864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Pilih skill
        ' or . = '
            Pilih skill
        ')]</value>
      <webElementGuid>a562e570-55b6-48dc-b787-0596886cd648</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
