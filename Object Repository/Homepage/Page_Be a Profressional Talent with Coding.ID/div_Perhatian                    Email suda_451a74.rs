<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Perhatian                    Email suda_451a74</name>
   <tag></tag>
   <elementGuidId>b42baa91-f069-4b58-81b5-09613a7c3819</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.container-alert</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[5]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>805fbb7e-386d-44cc-85e4-58bcdad99cd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container-alert</value>
      <webElementGuid>4df27210-23ea-4f1a-b5be-41f8e20975e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Perhatian
                    Email sudah terdaftar pada kelas tersebut
                    
                        Ok
                    

                    

                    
                </value>
      <webElementGuid>9351845f-9aec-415a-9e1e-162f0f3a3d1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;notif-alert-form notif-alert-form--show&quot;]/div[@class=&quot;container-alert&quot;]</value>
      <webElementGuid>06d0c209-254a-44ad-9b2b-eaea38bb14b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[5]/div</value>
      <webElementGuid>a9962279-b6ad-436d-b306-07f34548749b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Whatsapp'])[1]/following::div[2]</value>
      <webElementGuid>e288e279-7837-43df-915f-c82669f09009</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Bootcamp'])[3]/following::div[2]</value>
      <webElementGuid>fea4b915-0ffc-4586-b9f4-57798e216229</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div[5]/div</value>
      <webElementGuid>7d1f16ea-cb23-4c25-b9fe-c717e8377d07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    Perhatian
                    Email sudah terdaftar pada kelas tersebut
                    
                        Ok
                    

                    

                    
                ' or . = '
                    Perhatian
                    Email sudah terdaftar pada kelas tersebut
                    
                        Ok
                    

                    

                    
                ')]</value>
      <webElementGuid>9f27c720-5a8b-4754-9281-3fc40be335b5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
