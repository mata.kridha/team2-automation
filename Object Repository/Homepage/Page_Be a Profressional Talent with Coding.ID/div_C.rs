<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_C</name>
   <tag></tag>
   <elementGuidId>ad0ff05b-fd3b-49ff-b23a-b9fe778a9a12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.input-form.container-item</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>91bc395f-3f81-43a5-8639-50b0c03a7d01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>showOptions();</value>
      <webElementGuid>f48b401e-e9e0-4964-b509-7f6bea9c629d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-form container-item</value>
      <webElementGuid>8994f5a2-b834-4065-817d-34d9ea13d461</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                    C# 
                
        </value>
      <webElementGuid>345cce38-7d14-4706-a80a-99a2eef21906</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;main-form&quot;]/div[@class=&quot;track-form&quot;]/div[@class=&quot;page page-2&quot;]/div[@class=&quot;block-input-bio&quot;]/div[2]/div[@class=&quot;skill dropdown block-input-item&quot;]/div[@class=&quot;input-form container-item&quot;]</value>
      <webElementGuid>519db2bb-c6ce-4d9c-8f64-43bfbbb403bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>3f772d08-76d6-4dda-87fe-7c30b06e5722</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[10]/following::div[1]</value>
      <webElementGuid>b286920f-2b62-4bad-915a-b39a8fe38f3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[11]/preceding::div[4]</value>
      <webElementGuid>fdfab346-12db-4279-9722-af02521bc739</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/div[2]/div/div</value>
      <webElementGuid>f91c3877-8dec-4ac9-8bf6-ddf7687cb381</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                    C# 
                
        ' or . = '
            
                    C# 
                
        ')]</value>
      <webElementGuid>4284ee7e-e907-49b4-b4d3-d992c026aaa5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
