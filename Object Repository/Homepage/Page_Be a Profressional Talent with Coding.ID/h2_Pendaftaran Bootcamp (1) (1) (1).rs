<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Pendaftaran Bootcamp (1) (1) (1)</name>
   <tag></tag>
   <elementGuidId>8fac898b-a57f-4f1c-ae75-67b8a3709b2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.new_body1_semibold</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='FormBootcampContainer']/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>5f2b09b7-6650-426c-8d0d-6791ded2d19e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_body1_semibold</value>
      <webElementGuid>f0890996-0c35-44e6-8b44-ea9efc7b3c82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pendaftaran Bootcamp</value>
      <webElementGuid>be28b2ec-eab8-4500-9f42-f5c8ff9f5217</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FormBootcampContainer&quot;)/div[@class=&quot;header-bootcamp&quot;]/h2[@class=&quot;new_body1_semibold&quot;]</value>
      <webElementGuid>7772f0f7-2069-47e3-8d9a-6e7ed52a655c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div/h2</value>
      <webElementGuid>eb62251b-08be-46d6-a5c1-db7a7a9fe8c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Events'])[1]/following::h2[1]</value>
      <webElementGuid>f549c8e6-a0ca-4a9c-833a-990cd67550da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Semua Course'])[2]/following::h2[1]</value>
      <webElementGuid>3415b84c-e010-4f38-92a5-4445d902abbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih kelas yang ingin kamu ikuti'])[1]/preceding::h2[1]</value>
      <webElementGuid>f5fc2539-d22a-4baa-a879-2a033dff9cca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pendaftaran Bootcamp']/parent::*</value>
      <webElementGuid>3fda8d5e-4922-429d-8c8b-619401a19db6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/div/div/h2</value>
      <webElementGuid>326aa41a-6034-41a4-967c-320d9e8076d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Pendaftaran Bootcamp' or . = 'Pendaftaran Bootcamp')]</value>
      <webElementGuid>ff101998-9f02-4351-ae56-5822f301efaf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
