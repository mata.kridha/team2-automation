<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Login</name>
   <tag></tag>
   <elementGuidId>83b2d2c8-6456-41fd-8d88-3aef6f646efb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonLoginTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonLoginTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b607c197-8d17-4aef-805d-75d0afc394d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonLoginTrack</value>
      <webElementGuid>0c8b3024-dfd1-427c-b1e3-350108894b55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>9b0706e8-37ae-4be1-8fdf-3a9afbb0aec8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-lg btn-block</value>
      <webElementGuid>812ed224-bb49-4d5f-af79-c1d6b3232417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>4</value>
      <webElementGuid>e80c4c3b-c3dd-45b0-a521-fd7d00885a61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        Login
                                                    </value>
      <webElementGuid>0fdef5d3-0ce3-4e43-bb21-c31e2628f226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonLoginTrack&quot;)</value>
      <webElementGuid>daafad88-2e17-466a-a19c-1c0c2b2937a1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonLoginTrack']</value>
      <webElementGuid>7e0d3032-15e9-4ab5-8dfe-e504f6852535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::button[1]</value>
      <webElementGuid>8da995de-6c12-4051-a617-1443deafbd72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa kata sandi ?'])[1]/preceding::button[2]</value>
      <webElementGuid>76c0315f-32d5-4f74-a0bd-8a65b4206dad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>db110a41-ef7e-470f-aa7e-a4c65f8a1537</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonLoginTrack' and @type = 'submit' and (text() = '
                                                        Login
                                                    ' or . = '
                                                        Login
                                                    ')]</value>
      <webElementGuid>63151c17-bf28-4029-87b9-1db4fca75d1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
