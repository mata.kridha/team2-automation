<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Take                            this course</name>
   <tag></tag>
   <elementGuidId>32b5c841-633b-4a9c-8596-52aa7ecd68f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>df224205-1c9c-49fd-a3ca-8d5ea39de28b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>b98e4ad9-5a1a-457e-a69d-649f5a3b05e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>a6006822-15e9-4d59-a8cf-323e2cbc0fc6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:click</name>
      <type>Main</type>
      <value>addToCart</value>
      <webElementGuid>834301b4-68e4-4716-8a87-8c081adde4c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:loading.class</name>
      <type>Main</type>
      <value>invisible</value>
      <webElementGuid>4ea3f42e-fef8-4436-b47c-7ad485b857af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                            this course
                        </value>
      <webElementGuid>252c0090-922e-43ec-b344-3658059037b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>5bfcf3f6-09bc-45fa-adfc-6b59173c8d1a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      <webElementGuid>1043eb39-6f96-407a-968f-fa5d5331b57b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Starter to Express JS API'])[1]/following::button[1]</value>
      <webElementGuid>bd315847-864d-44fb-bb17-ad3afe59df27</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loading...'])[1]/preceding::button[1]</value>
      <webElementGuid>ae08f8a6-a71f-44f0-9905-5b5745f78d94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>4dc89c82-edaf-4c2d-9eca-45394d47b438</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/button</value>
      <webElementGuid>341783c0-5700-41a8-9b37-7b48c7fe6feb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                            this course
                        ' or . = 'Take
                            this course
                        ')]</value>
      <webElementGuid>b9175c19-18f1-44d7-ab56-a0127495bd9e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
