<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Course</name>
   <tag></tag>
   <elementGuidId>d1596373-b792-485d-947b-c85595da1475</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(3) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7be7faed-3667-48b4-9408-e39da3d37dc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/course</value>
      <webElementGuid>cabc9236-f728-4a86-b5b5-2423faa986a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Course</value>
      <webElementGuid>230184e7-bf16-40ab-bae1-fafbc18ebd91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[3]/a[1]</value>
      <webElementGuid>d7b7ca26-18f4-48fe-88ce-3d624f75ec67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[3]/a</value>
      <webElementGuid>9631810d-7966-4fe4-990e-c5b3cc41a750</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Course')]</value>
      <webElementGuid>1a296695-db2e-4fc0-a054-b8a518ccb72e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bootcamp'])[1]/following::a[1]</value>
      <webElementGuid>a6bcbdb0-5d95-47d9-b845-f7f0582b7e8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle navigation'])[1]/following::a[2]</value>
      <webElementGuid>3c9c89ec-2c06-4b90-b606-9fbe44aae17a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[1]/preceding::a[1]</value>
      <webElementGuid>34bce659-6972-4420-990c-2b6f67f59c40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/preceding::a[2]</value>
      <webElementGuid>9f1e0cfb-d190-4d83-845c-a45e3dccf3cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Course']/parent::*</value>
      <webElementGuid>a00d5c43-b2c3-4ca2-b0c0-be8af4e246c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/course')]</value>
      <webElementGuid>0265e51d-76b0-4547-8b72-0f6c884cbe60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>08781b7f-7e5a-49ad-bdfc-47afb3bd3fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/course' and (text() = 'Course' or . = 'Course')]</value>
      <webElementGuid>5b681e68-e32e-4075-9e19-1d380a570202</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
