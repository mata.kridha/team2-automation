<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_Rp 5.000.000_imageHeader</name>
   <tag></tag>
<<<<<<< HEAD
   <elementGuidId>bc526876-1808-4214-a126-9e8a866b2b60</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='containerCard']/div/a/div/img)[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>8f7977c1-73e2-4f91-99f7-567e9b0d1a19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://storage.googleapis.com/codingid-storage-bucket/storage/uploads/lp-course-image/1663321645ezgif.com-gif-maker (3).webp</value>
      <webElementGuid>960b8419-e928-4d58-8fba-e4e7d39da967</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>imageHeader</value>
      <webElementGuid>7fcf8e3b-2f3c-4336-94a4-cfeac81eda36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>course banner</value>
      <webElementGuid>6eb424a7-5e85-4c16-af01-6f8f5e8c9492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>course banner</value>
      <webElementGuid>fdea013b-c8dc-4f68-977c-3b66a5589b51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;containerCard&quot;]/div[@class=&quot;cardOuter&quot;]/a[1]/div[@class=&quot;cardHeader&quot;]/img[@class=&quot;imageHeader&quot;]</value>
      <webElementGuid>0c95ae7d-eba7-4c93-85de-81486aed06ba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='containerCard']/div/a/div/img)[3]</value>
      <webElementGuid>069e4472-7281-4375-98f0-aed161637990</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='course banner'])[3]</value>
      <webElementGuid>32105606-11e9-42ae-bc97-039e3d3d1719</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/a/div/img</value>
      <webElementGuid>5be713bb-1fd1-47b9-b719-220254a13ffb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'https://storage.googleapis.com/codingid-storage-bucket/storage/uploads/lp-course-image/1663321645ezgif.com-gif-maker (3).webp' and @alt = 'course banner' and @title = 'course banner']</value>
      <webElementGuid>f08db5f0-9a0d-4120-ae8f-ddab9ae216d0</webElementGuid>
=======
   <elementGuidId>0e54ae10-9dd7-4646-a045-6f1c38c9545c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='containerCard']/div/a/div/img)[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>fd198008-65b4-4b5f-a12b-daac09f49584</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://storage.googleapis.com/codingid-storage-bucket/storage/uploads/lp-course-image/1663321645ezgif.com-gif-maker (3).webp</value>
      <webElementGuid>62b20fee-fbad-4225-ae04-138624fd0302</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>imageHeader</value>
      <webElementGuid>57d9926e-f178-49be-9024-90fa6d22edf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>course banner</value>
      <webElementGuid>d371a837-c2b1-44fb-b819-5b16d6bee4a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>course banner</value>
      <webElementGuid>c9b1348b-8c23-4a46-80ec-95b67bfbe139</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;containerCard&quot;]/div[@class=&quot;cardOuter&quot;]/a[1]/div[@class=&quot;cardHeader&quot;]/img[@class=&quot;imageHeader&quot;]</value>
      <webElementGuid>6efcb59a-5ab5-47e1-a9e0-e8b013b72db8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='containerCard']/div/a/div/img)[3]</value>
      <webElementGuid>ddf0e56f-bffd-4970-b9bb-cd3af904d192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>(//img[@alt='course banner'])[3]</value>
      <webElementGuid>916f5433-3968-4211-aad0-63a833a094ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/div/a/div/img</value>
      <webElementGuid>1fc0dd38-25f2-417e-87de-c4feb0dfe197</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = 'https://storage.googleapis.com/codingid-storage-bucket/storage/uploads/lp-course-image/1663321645ezgif.com-gif-maker (3).webp' and @alt = 'course banner' and @title = 'course banner']</value>
      <webElementGuid>cf8cb2d0-48e5-4583-87d6-7498cd535c24</webElementGuid>
>>>>>>> branch 'Develop-Automation' of https://gitlab.com/Fajarnugraha/test-case-automation-final-project.git
   </webElementXpaths>
</WebElementEntity>
