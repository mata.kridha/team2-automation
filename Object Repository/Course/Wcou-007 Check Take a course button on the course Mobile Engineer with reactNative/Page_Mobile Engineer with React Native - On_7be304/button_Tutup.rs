<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tutup</name>
   <tag></tag>
   <elementGuidId>4eec271e-973e-47e8-87cb-55fde72d46a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>52de446b-bd79-4fc0-a52a-3cd8d632c0da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click</name>
      <type>Main</type>
      <value>modalOpen = !modalOpen</value>
      <webElementGuid>46b20ff2-b8cd-4e57-a844-0852fdbc8081</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>92ff03a7-5e55-4720-9537-f7ace14e5fb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>close</value>
      <webElementGuid>4f4f5827-b7f7-4259-ade8-662bd4dc0d04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dismiss</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>5d4d87de-d426-416a-ab03-6098b7864417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Tutup </value>
      <webElementGuid>840a13b7-137b-4b00-b4a5-ae7f6f645488</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot; Modal_Success&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-header&quot;]/button[@class=&quot;close&quot;]</value>
      <webElementGuid>39615535-1ec3-408e-a4fe-ebc7947f61af</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[8]</value>
      <webElementGuid>c4c36269-a3a1-4e01-bbe8-c754d9399177</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id=' Modal_Success']/div/div/button</value>
      <webElementGuid>89e95211-610e-4332-97d9-6f333712214c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MySQL'])[2]/following::button[2]</value>
      <webElementGuid>ad62326f-f0ae-49c6-b816-1a02194d5fd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course berhasil ditambahkan ke pembelian!'])[1]/preceding::button[1]</value>
      <webElementGuid>a91aae9d-1ef2-4c3f-b95f-72723c05bbde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[3]/preceding::button[1]</value>
      <webElementGuid>67602ad3-551b-4796-9643-5852618cce3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/button</value>
      <webElementGuid>aa2bde3e-9f02-4dc1-9db2-72a10c762878</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = ' Tutup ' or . = ' Tutup ')]</value>
      <webElementGuid>91b673cd-821d-4fcf-92b5-bdd22a61ce3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
