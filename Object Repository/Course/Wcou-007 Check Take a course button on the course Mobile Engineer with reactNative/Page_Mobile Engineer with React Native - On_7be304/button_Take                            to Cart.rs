<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Take                            to Cart</name>
   <tag></tag>
<<<<<<< HEAD
   <elementGuidId>fdcecc0e-5613-4349-be49-142883b3835c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>82640f35-00d8-413c-91a3-e57997063afd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>eb07b035-0c2d-46ff-849f-3376c0877a01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>57431293-07a5-4c98-aa6f-eef89dea3abf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                            to Cart
                        </value>
      <webElementGuid>9b2caad6-898f-45d8-94c2-4bd05b68d32f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/a[1]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>f6c1ddae-0516-44d2-bbfd-567cf9980083</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      <webElementGuid>692b2772-1f0f-455c-a742-2524e969bae0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[1]/following::button[1]</value>
      <webElementGuid>1f4f1afa-388e-4811-ae2a-6c714ad4814a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>56ba634b-bd00-4f76-b6b3-e6a3727d0cad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[2]/preceding::button[1]</value>
      <webElementGuid>f3dcc6d5-98cf-458a-b4ca-ae57bd509d2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>b0cfbc8b-ec8e-4e4a-a80c-f12a854393a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                            to Cart
                        ' or . = 'Take
                            to Cart
                        ')]</value>
      <webElementGuid>61cbb445-5a73-4034-a2ba-740a2a2433b6</webElementGuid>
=======
   <elementGuidId>8e0edb93-ee1c-46d2-a96f-d5bb4d3331e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>581f9266-bebf-4191-a7d0-2ac291adbfac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>a48eaff7-7876-441b-a09d-7c9b704a9bca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>ece81dfb-d539-4577-b272-85c72470af4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                            to Cart
                        </value>
      <webElementGuid>a2e805e6-8434-480a-88aa-6a1b6ec11a04</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/a[1]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>653584b1-73d3-443b-9ddc-5af696946704</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/a/button</value>
      <webElementGuid>cf4b0f52-d4a4-4ce2-badb-e4b345c55684</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[1]/following::button[1]</value>
      <webElementGuid>f3107ed2-51bf-4164-bc35-1baed1688f33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>4b350efc-8464-4cdd-b95e-47a06d5bafd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[2]/preceding::button[1]</value>
      <webElementGuid>59deb1aa-675c-41db-9eb4-1c43275106ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>d518d944-8b15-4a59-a4f9-c56b9812d84a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                            to Cart
                        ' or . = 'Take
                            to Cart
                        ')]</value>
      <webElementGuid>090b5211-99d7-4693-bde9-b5d36df7d2c2</webElementGuid>
>>>>>>> branch 'Develop-Automation' of https://gitlab.com/Fajarnugraha/test-case-automation-final-project.git
   </webElementXpaths>
</WebElementEntity>
