<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Take                this course</name>
   <tag></tag>
   <elementGuidId>9e638abf-5191-4359-89f0-a578f4fc8116</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-light.buttonLoad</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>49774e28-c518-4875-a72c-ae3ae394e283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>wire:click</name>
      <type>Main</type>
      <value>addToCart</value>
      <webElementGuid>6a3c9c56-9eb6-4ea0-aa9b-433c95b0df55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-light buttonLoad</value>
      <webElementGuid>1cf67e8d-625b-4b68-a453-490b7b73ba2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-bind:class</name>
      <type>Main</type>
      <value>modalOpen ? 'modal-open' : ''</value>
      <webElementGuid>0ff4aa3a-05f8-46af-b33a-8cfd43b31959</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Take
                this course
            </value>
      <webElementGuid>2ec824d3-776d-4be1-8f7e-17bbadff256b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)/div[@class=&quot;textJumbotron&quot;]/button[@class=&quot;btn btn-light buttonLoad&quot;]</value>
      <webElementGuid>7b61537d-f145-4695-a125-7fbf638f6454</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/button</value>
      <webElementGuid>b3a489c7-1d43-4a66-85f0-0d8e84c26870</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Frontend Engineer with ReactJS'])[1]/following::button[1]</value>
      <webElementGuid>0df07ea2-4533-46c7-b717-ed55a5459754</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::button[1]</value>
      <webElementGuid>bdbcabdc-59fd-48e7-aeb4-0e03c5085127</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Frontend Engineer with ReactJS'])[2]/preceding::button[1]</value>
      <webElementGuid>501e4619-f03f-463b-9a65-872a12e06303</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/button</value>
      <webElementGuid>850a4e6f-0c23-4947-82a7-39346b0d24f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Take
                this course
            ' or . = 'Take
                this course
            ')]</value>
      <webElementGuid>863808af-ca3d-4563-8cda-220089551f2e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
