<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_inlineCheckbox1</name>
   <tag></tag>
   <elementGuidId>1d745c94-082a-4be5-a84a-25d5cc9039e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inlineCheckbox1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#inlineCheckbox1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>2b8408ab-0628-4516-a942-1d6a3d1a3d7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>d75422a3-7b76-4fe0-9081-13ffd25b4898</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>06e48165-60f5-47a6-8d2a-fe813d8cb92e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inlineCheckbox1</value>
      <webElementGuid>6ccb18c5-3c98-43e5-8e37-7fc86c587cb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>option1</value>
      <webElementGuid>40150116-338e-41f0-815d-2e09a0c78e8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inlineCheckbox1&quot;)</value>
      <webElementGuid>c0c59b38-9dec-48a4-8f35-bd26a5b8fe2f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inlineCheckbox1']</value>
      <webElementGuid>a4ad4caf-8ebe-40c2-8e74-53de5b0a5381</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/input</value>
      <webElementGuid>17592d5c-6041-443b-85a2-806efac797d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'inlineCheckbox1']</value>
      <webElementGuid>49aacbba-c9fb-4c7a-b0c1-91cdcccce09b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
