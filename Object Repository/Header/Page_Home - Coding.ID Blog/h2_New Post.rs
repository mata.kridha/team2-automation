<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_New Post</name>
   <tag></tag>
   <elementGuidId>5aa68992-51a4-4e4e-a3e6-db499fa11842</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/div/section[2]/div/div/div/div[2]/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2.elementor-heading-title.elementor-size-default</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>a546c20a-cc6a-4d93-b2d0-a545e073087c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>elementor-heading-title elementor-size-default</value>
      <webElementGuid>7c61a2a6-5dfe-4f1a-b0b1-0bfb9d1105cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Post</value>
      <webElementGuid>4d2ab914-f10b-4de3-83c6-9515bb583ff1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;ast-container&quot;]/div[@class=&quot;elementor elementor-101&quot;]/section[@class=&quot;elementor-section elementor-top-section elementor-element elementor-element-66bb443a elementor-section-boxed elementor-section-height-default elementor-section-height-default&quot;]/div[@class=&quot;elementor-container elementor-column-gap-default&quot;]/div[@class=&quot;elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4123552c&quot;]/div[@class=&quot;elementor-widget-wrap elementor-element-populated&quot;]/div[@class=&quot;elementor-element elementor-element-58f23963 elementor-widget elementor-widget-heading&quot;]/div[@class=&quot;elementor-widget-container&quot;]/h2[@class=&quot;elementor-heading-title elementor-size-default&quot;]</value>
      <webElementGuid>8132ec11-ba7c-4b46-965e-7a46b13c158f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/div/section[2]/div/div/div/div[2]/div/h2</value>
      <webElementGuid>b5ebb9da-5684-4320-bbde-bd2106d36bc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact'])[1]/following::h2[1]</value>
      <webElementGuid>85ced48b-d066-49eb-a6e5-c041e673c20a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::h2[1]</value>
      <webElementGuid>37724b73-316e-41de-812f-2751b164c267</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Info Technology'])[1]/preceding::h2[1]</value>
      <webElementGuid>c6c653fa-5493-4726-b74a-f487c003dab9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New Post']/parent::*</value>
      <webElementGuid>f972d687-ac29-4d14-ba44-a5064d5c2ad9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>d5fb691d-f36d-45b6-b80c-bac87ecd8386</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'New Post' or . = 'New Post')]</value>
      <webElementGuid>a3799576-ae5b-454a-b65b-55ea6d777af1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
