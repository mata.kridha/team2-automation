<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_No Result</name>
   <tag></tag>
   <elementGuidId>d32b2488-4ab8-4ee9-8007-c4ccd66f05b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='searcning-list']/div/div[2]/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>79fa5256-c8f2-42da-9239-862007557454</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>No Result</value>
      <webElementGuid>dbfd4a06-f685-4e81-ba99-7774255637dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;searcning-list&quot;)/div[1]/div[@class=&quot;listSearching&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/p[1]</value>
      <webElementGuid>c6c498d4-1f16-4467-8975-6189e75b72ad</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='searcning-list']/div/div[2]/div/div/p</value>
      <webElementGuid>65a71cb5-a6b4-47a2-89d3-5b9f5242ddc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Match'])[1]/following::p[1]</value>
      <webElementGuid>2fded48f-d04c-48e0-9bbf-17b77aabd633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun'])[3]/following::p[1]</value>
      <webElementGuid>4c5f07ca-1bc2-423a-97a2-07592531d3c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[1]/preceding::p[2]</value>
      <webElementGuid>aa1fdcdf-b9b9-4a33-afb2-25d9b462cf1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advance Level'])[2]/preceding::p[3]</value>
      <webElementGuid>bad70f7f-bd59-460b-bbe2-07697195e0c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='No Result']/parent::*</value>
      <webElementGuid>8f48d04b-add0-4335-b03c-920fc32d4dc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>12545a67-80f1-4f14-9e9d-feca0abf3a50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'No Result' or . = 'No Result')]</value>
      <webElementGuid>fabd1409-ae21-4e21-a908-98590bb67e9f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
