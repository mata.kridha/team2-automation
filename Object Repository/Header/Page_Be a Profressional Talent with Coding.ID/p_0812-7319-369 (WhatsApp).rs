<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_0812-7319-369 (WhatsApp)</name>
   <tag></tag>
   <elementGuidId>0dc5d55b-84e9-423b-8c4d-5722dffafa00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='wm-footer']/div/div/div/aside[3]/p[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>bd20a80b-0d26-442d-8ce4-942a4d536ae6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>0812-7319-369 (WhatsApp)</value>
      <webElementGuid>e489edc8-8bbd-438a-b3d8-873961eefc90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wm-footer&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4 col-sm-12 col-12&quot;]/p[2]</value>
      <webElementGuid>72b4da08-92bd-409a-a73f-8bad3a8db249</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='wm-footer']/div/div/div/aside[3]/p[2]</value>
      <webElementGuid>d64c0516-8203-4821-a842-03e6c8e21925</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[5]/following::p[2]</value>
      <webElementGuid>0bce3c3d-76c2-48ac-9e16-e1367a31bd16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up'])[1]/following::p[2]</value>
      <webElementGuid>d9819106-a4e7-48ce-83be-12545b406478</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[6]/preceding::p[1]</value>
      <webElementGuid>0eafbe2c-821a-4799-a30d-e9096e4298da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bootcamp'])[5]/preceding::p[1]</value>
      <webElementGuid>f1517e2b-615e-4e23-a48c-ac5bf751f268</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='0812-7319-369 (WhatsApp)']/parent::*</value>
      <webElementGuid>6c1ad00d-80c1-4cb2-a153-f7aee1050ce5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside[3]/p[2]</value>
      <webElementGuid>705cbb31-0511-4aa8-a4b1-e1e695ed194c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '0812-7319-369 (WhatsApp)' or . = '0812-7319-369 (WhatsApp)')]</value>
      <webElementGuid>2c97a8bf-dfd5-45f5-b2d9-c082f88f90ec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
