<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Kontak (1)</name>
   <tag></tag>
   <elementGuidId>da4729b8-b147-46a8-99e3-867be455fa76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>3be1a36c-66c3-4713-9bb4-72dce53d86b2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>listSiteMap</value>
      <webElementGuid>04338d5a-f55a-42a0-a50b-214fc857f860</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/contact</value>
      <webElementGuid>224476d1-387a-44da-9283-54db781f107b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                Kontak</value>
      <webElementGuid>f65e8347-f181-4d66-a1f0-84dee45fda8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wm-footer&quot;)/div[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4 col-sm-12 col-12&quot;]/div[@class=&quot;row&quot;]/ul[@class=&quot;col-md-6 col-sm-6 col-xs-6&quot;]/li[1]/a[@class=&quot;listSiteMap&quot;]</value>
      <webElementGuid>3bf7b0a2-5945-4f63-846c-2783ecefc617</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='wm-footer']/div/div/div/aside[2]/div[2]/ul[2]/li/a</value>
      <webElementGuid>6716c2de-5525-47f1-92d9-2ae41ee97261</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Kontak')])[4]</value>
      <webElementGuid>7e600b13-3b62-4d0b-bdae-0dea449a1f17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Course'])[5]/following::a[2]</value>
      <webElementGuid>49408596-512e-4c54-9a83-4004eac4262b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/preceding::a[1]</value>
      <webElementGuid>6eba4c89-38fa-4a6d-b89f-7181675f44e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign Up'])[1]/preceding::a[2]</value>
      <webElementGuid>2a72531f-1f16-4dc1-a125-b8d84bf80725</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/contact')])[5]</value>
      <webElementGuid>dd7d6804-731b-47bc-a42a-5af377e8fa56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul[2]/li/a</value>
      <webElementGuid>bd994ddf-f27d-4794-bb3d-f78fc7a6457b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/contact' and (text() = '
                                                Kontak' or . = '
                                                Kontak')]</value>
      <webElementGuid>9e23a4b1-8297-4717-a522-2a5975273268</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
