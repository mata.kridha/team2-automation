<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Cancel                        Save Changes</name>
   <tag></tag>
   <elementGuidId>ad70585f-7540-4cb0-b21b-91a2241ae31f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-footer.text-right</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dfd978a7-94cd-4baa-aba5-57ca0b619a80</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-footer text-right</value>
      <webElementGuid>25567dd5-9e4a-440e-834b-eb06dff72f0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Cancel
                        Save Changes
                    </value>
      <webElementGuid>754e25ac-e596-4a7d-9019-a2b84eaf17cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;card-footer text-right&quot;]</value>
      <webElementGuid>456663b7-c31a-4e4d-9f18-c9624e9a6e3f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div[4]</value>
      <webElementGuid>b4734b99-5e43-4f9a-8dca-948a063b68cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirmation Password'])[1]/following::div[1]</value>
      <webElementGuid>f0dcd251-3504-4348-8b21-888b083ea875</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Password'])[1]/following::div[2]</value>
      <webElementGuid>17c6086f-106b-48fb-bde8-2f71db0663de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[4]</value>
      <webElementGuid>b21ed599-73b3-4b72-b8a3-6e8968c7b35f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                        Cancel
                        Save Changes
                    ' or . = '
                        
                        Cancel
                        Save Changes
                    ')]</value>
      <webElementGuid>2e509590-afe0-4f8c-8e2d-d13d2f17947c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
