<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ul_Quality Assurance Engineer Class        _f84527</name>
   <tag></tag>
   <elementGuidId>b99719a3-639a-4469-a738-074d9ea1b7aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#classRow</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='classRow']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>f2ca39dc-028f-4db8-bd8a-4b2e7f69b55a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row </value>
      <webElementGuid>7fbde3bd-b009-476f-aa5b-597b909ee48a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>classRow</value>
      <webElementGuid>86e5a49c-8d8e-4944-977c-760ab347f193</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    
                    
                        
                            
                                
                            
                        
                        
                            
                                Quality Assurance Engineer Class
                                
                            

                                                            
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea... 
                            
                            
                                
                                    Lihat Program 
                                
                            
                        
                    
                
                        </value>
      <webElementGuid>0f21ac86-6241-4e74-95a9-9bf6552b2d8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]/ul[@id=&quot;classRow&quot;]</value>
      <webElementGuid>eedb414d-6ec4-4f45-aaa1-d32b84fcfcfe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//ul[@id='classRow']</value>
      <webElementGuid>9da66fec-120c-4cf0-b625-1c9d1701d4e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']/div/ul</value>
      <webElementGuid>2380b26c-87f6-4ca3-b1f8-47c894f72b03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Programs'])[1]/following::ul[1]</value>
      <webElementGuid>53d569ec-b383-4937-bf5d-807846b9335b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Programs'])[1]/following::ul[1]</value>
      <webElementGuid>81619d7e-c99c-470c-b43e-738f73318edd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul</value>
      <webElementGuid>663445cf-1bfb-497f-ae78-fe3be6906c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[@id = 'classRow' and (text() = '
                                    
                    
                        
                            
                                
                            
                        
                        
                            
                                Quality Assurance Engineer Class
                                
                            

                                                            
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea... 
                            
                            
                                
                                    Lihat Program 
                                
                            
                        
                    
                
                        ' or . = '
                                    
                    
                        
                            
                                
                            
                        
                        
                            
                                Quality Assurance Engineer Class
                                
                            

                                                            
                                    Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Bea... 
                            
                            
                                
                                    Lihat Program 
                                
                            
                        
                    
                
                        ')]</value>
      <webElementGuid>6f88484f-0a10-4451-97d2-740b856ffe6c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
