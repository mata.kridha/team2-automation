<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Lihat Program (1)</name>
   <tag></tag>
   <elementGuidId>2792ece5-dda1-4144-a711-933adcc3cdd0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='classRow']/div/div[2]/a/h6)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>6d62de38-1769-4cc8-b090-a9f7f3fd0775</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleProgram</value>
      <webElementGuid>6b42d2f2-4c3e-45bb-9947-c6ebc2d8397f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                    Lihat Program 
                                </value>
      <webElementGuid>b7de53af-ce84-4619-bddc-8f377b09f6ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)/div[1]/ul[@id=&quot;classRow&quot;]/li[@id=&quot;classRow&quot;]/div[1]/div[2]/a[1]/h6[@class=&quot;titleProgram&quot;]</value>
      <webElementGuid>144a4fc0-a3e1-4f52-a044-bb0ea6a07f86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='classRow']/div/div[2]/a/h6)[2]</value>
      <webElementGuid>18a63226-fbdd-40e3-94d2-0a429a91c94c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Fullstack Engineer hanya dalam 2 bulan dengan Jaminan Kerja ...'])[1]/following::h6[1]</value>
      <webElementGuid>fe284740-1f8f-4262-b829-2069f82f30c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[2]/following::h6[2]</value>
      <webElementGuid>4c21120b-fde0-4ec6-906b-0177037f6ae5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gabung CODING.ID Newsletter'])[1]/preceding::h6[1]</value>
      <webElementGuid>7ffae8d7-7a55-415e-bc30-b6d78cba88cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Format email tidak valid'])[1]/preceding::h6[1]</value>
      <webElementGuid>5807a0bc-19d7-445d-a7e3-ae7586a07cb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/div/div[2]/a/h6</value>
      <webElementGuid>80a45787-d062-40e2-86db-d6863eafab35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                    Lihat Program 
                                ' or . = '
                                    Lihat Program 
                                ')]</value>
      <webElementGuid>ec894633-e5c5-4824-bd3b-3b13d385dfcd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
