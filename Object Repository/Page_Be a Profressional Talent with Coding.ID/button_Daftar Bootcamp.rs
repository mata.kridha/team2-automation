<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar Bootcamp</name>
   <tag></tag>
   <elementGuidId>4e30df72-4156-4a43-b86d-a9c830ef223b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='next-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#next-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>47cdb30e-22b3-4ebe-990e-3cc091a95409</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bootcamp</name>
      <type>Main</type>
      <value>Quality Assurance Engineer Class</value>
      <webElementGuid>32e7daf3-aae9-425d-9d14-45b976b54826</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>@click.prevent</name>
      <type>Main</type>
      <value>$store.dataForm.changeClassShow(2);$store.dataForm.classId = 4;$store.dataForm.changeClass()</value>
      <webElementGuid>0c6b619b-130e-4f51-8bbb-8e4af8333024</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>daftarBootcamp daftarBootcamp--see-all disabled</value>
      <webElementGuid>3f3f411d-ef8f-40dd-b1af-7139192de15b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Daftar Bootcamp</value>
      <webElementGuid>bb17303d-9c4b-453a-b6ee-116f826ae675</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;next-button&quot;)</value>
      <webElementGuid>d1747841-1c66-48fd-beb4-1f5ee3d807bf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x-on:click</name>
      <type>Main</type>
      <value>$store.dataForm.validatePage($store.dataForm.activePage)</value>
      <webElementGuid>7a05a271-f495-430b-a64a-a3a370ccede6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>next-button</value>
      <webElementGuid>eea55dc1-e03b-437b-920b-63d86216c14d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>:class</name>
      <type>Main</type>
      <value>$store.dataForm.classId == 0 ||
                            ($store.dataForm.activePage == 1 &amp;&amp; !$store.dataForm.validPage2) || ($store.dataForm
                                .activePage == 2 &amp;&amp; !$store.dataForm.validPage3) ?
                            'disabled' : ''</value>
      <webElementGuid>892b9daf-7625-4f3c-b42d-b7b1dcd0ec8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='next-button']</value>
      <webElementGuid>7d9eb185-1c86-4f17-a9b4-4b08cc005128</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='FormBootcampContainer']/div[4]/div/button[2]</value>
      <webElementGuid>436bcdfd-733a-4b85-980d-faa30ae69886</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sebelumnya'])[1]/following::button[1]</value>
      <webElementGuid>58871a61-8bea-49a8-add0-624004d5582a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Kode milik ', &quot;'&quot;, '', &quot;'&quot;, ' diterapkan')])[1]/following::button[2]</value>
      <webElementGuid>0480a78a-850b-4714-b96b-0794b659ea5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Whatsapp'])[1]/preceding::button[1]</value>
      <webElementGuid>0d6bbda0-8fb1-42b3-a811-2e712f2db8b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Perhatian'])[1]/preceding::button[1]</value>
      <webElementGuid>f3ebd724-8b29-4175-b84c-22aff16d4497</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/button[2]</value>
      <webElementGuid>29821354-4f78-4dea-8f7f-f5c88bd63c9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'next-button' and (text() = 'Daftar Bootcamp' or . = 'Daftar Bootcamp')]</value>
      <webElementGuid>d6abd231-10a8-430a-8550-3be10255590a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[2]/div/ul/li/div[2]/button</value>
      <webElementGuid>e455bd4c-8a85-4853-b26c-0f67e43fc8dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay Later'])[1]/following::button[1]</value>
      <webElementGuid>86cf043e-6e4a-495a-bc91-8a173e3397af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BOOTCAMP'])[2]/preceding::button[1]</value>
      <webElementGuid>7f9f99a1-dd95-4de0-adc0-d3b0fb76a49b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[3]/preceding::button[1]</value>
      <webElementGuid>21a4194b-0d90-4861-b9bb-792cf81eed10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar Bootcamp']/parent::*</value>
      <webElementGuid>bfc7de1e-e02f-405e-89c3-4eddc55d8c24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/div[2]/button</value>
      <webElementGuid>698a39de-b4e8-4dee-8836-17aaa1adeb21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
                                        Daftar Bootcamp
                                    ' or . = '
                                        Daftar Bootcamp
                                    ')]</value>
      <webElementGuid>5b2226ec-656c-433e-a26f-c534ada97c8b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
